<?php

/**
 * Class m050220_160420_create_content_article_tag_map_table
 */
class m050220_160420_create_content_article_tag_map_table extends \common\db\Migration
{
    const CONTENT_ARTICLE_TAG_MAP_TABLE = '{{%content_article_tag_map}}';

    /**
     * {@inheritdoc}
     * @throws \yii\base\Exception
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        if($this->checkTable(self::CONTENT_ARTICLE_TAG_MAP_TABLE))
        {
            $this->createTable(self::CONTENT_ARTICLE_TAG_MAP_TABLE, [
                'id' => $this->primaryKey(),
                'article_id' => $this->integer()->notNull(),
                'tag_id' => $this->integer()->notNull(),
            ], $tableOptions);

            $this->addForeignKey(
                'chain_to_content_article',
                'content_article_tag_map',
                'article_id',
                'content_article',
                'id',
                'cascade'
            );
            $this->addForeignKey(
                'chain_to_content_tag',
                'content_article_tag_map',
                'tag_id',
                'content_tag',
                'id',
                'cascade'
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::CONTENT_ARTICLE_TAG_MAP_TABLE);
    }
}
