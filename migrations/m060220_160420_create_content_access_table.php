<?php

/**
 * Class m060220_160420_create_content_access_table
 */
class m060220_160420_create_content_access_table extends \common\db\Migration
{
    const CONTENT_ACCESS_TABLE = '{{%content_access}}';

    /**
     * {@inheritdoc}
     * @throws \yii\base\Exception
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        if($this->checkTable(self::CONTENT_ACCESS_TABLE))
        {
            $this->createTable(self::CONTENT_ACCESS_TABLE, [
                'id' => $this->primaryKey(),
                'title' => $this->json()
            ], $tableOptions);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::CONTENT_ACCESS_TABLE);
    }
}
