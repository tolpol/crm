<?php

/**
 * Class m030120_160420_create_content_tag_table
 */
class m030120_160420_create_content_tag_table extends \common\db\Migration
{
    const CONTENT_TAG_TABLE = '{{%content_tag}}';

    /**
     * {@inheritdoc}
     * @throws \yii\base\Exception
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        if($this->checkTable(self::CONTENT_TAG_TABLE))
        {
            $this->createTable(self::CONTENT_TAG_TABLE, [
                'id' => $this->primaryKey(),
                'title' => $this->json(),
            ], $tableOptions);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::CONTENT_TAG_TABLE);
    }
}
