<?php

/**
 * Class m050220_160420_create_state_table
 */
class m070220_160420_create_content_state_table extends \common\db\Migration
{
    const CONTENT_STATE_TABLE = '{{%content_state}}';

    /**
     * {@inheritdoc}
     * @throws \yii\base\Exception
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        if($this->checkTable(self::CONTENT_STATE_TABLE))
        {
            $this->createTable(self::CONTENT_STATE_TABLE, [
                'id' => $this->primaryKey(),
                'title' => $this->json()
            ], $tableOptions);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::CONTENT_STATE_TABLE);
    }
}
