<?php

/**
 * Class m050220_160420_create_content_article_category_map_table
 */
class m050220_160420_create_content_article_category_map_table extends \common\db\Migration
{
    const CONTENT_ARTICLE_CATEGORY_MAP_TABLE = '{{%content_article_category_map}}';

    /**
     * {@inheritdoc}
     * @throws \yii\base\Exception
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        if($this->checkTable(self::CONTENT_ARTICLE_CATEGORY_MAP_TABLE))
        {
            $this->createTable(self::CONTENT_ARTICLE_CATEGORY_MAP_TABLE, [
                'id' => $this->primaryKey(),
                'article_id' => $this->integer()->notNull(),
                'category_id' => $this->integer()->notNull(),
                'created_at' => $this->integer()->notNull(),
                'updated_at' => $this->integer()->notNull(),
                'created_by' => $this->integer()->notNull(),
                'updated_by' => $this->integer()->notNull(),
            ], $tableOptions);

            $this->addForeignKey(
                'chain_to_content_article',
                'content_article_category_map',
                'article_id',
                'content_article',
                'id',
                'cascade'
            );
            $this->addForeignKey(
                'chain_to_content_category',
                'content_article_category_map',
                'category_id',
                'content_category',
                'id',
                'cascade'
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::CONTENT_ARTICLE_CATEGORY_MAP_TABLE);
    }
}
