<?php

/**
 * Class m030120_133120_create_content_article_table
 */
class m030120_133120_create_content_article_table extends \common\db\Migration
{
    const CONTENT_ARTICLE_TABLE = '{{%content_article}}';

    /**
     * {@inheritdoc}
     * @throws \yii\base\Exception
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        if($this->checkTable(self::CONTENT_ARTICLE_TABLE))
        {
            $this->createTable(self::CONTENT_ARTICLE_TABLE, [
                'id' => $this->primaryKey(),
                'title' => $this->string(255)->notNull(),
                'alias' => $this->string(255)->notNull(),
                'text' => $this->text()->notNull(),

                'image1' => $this->string(255)->notNull(),
                'image2' => $this->string(255)->notNull(),
                'files' => $this->json()->notNull(),
                'template' => $this->integer(),

                'meta_title' => $this->string(100),
                'meta_desc' => $this->text(),

                'author' => $this->integer(),
                'published_at' => $this->integer(),

                'access' => $this->tinyInteger(),
                'lang' => $this->integer(),
                'state' => $this->tinyInteger()->defaultValue(0),

                'commenting' => $this->integer()->defaultValue(0),

                'created_at' => $this->integer()->notNull(),
                'updated_at' => $this->integer()->notNull(),
                'created_by' => $this->integer()->notNull(),
                'updated_by' => $this->integer()->notNull()
            ], $tableOptions);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::CONTENT_ARTICLE_TABLE);
    }
}
