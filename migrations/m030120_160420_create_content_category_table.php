<?php

/**
 * Class m030120_160420_create_category_table
 */
class m030120_160420_create_content_category_table extends \common\db\Migration
{
    const CONTENT_CATEGORY_TABLE = '{{%content_category}}';

    /**
     * {@inheritdoc}
     * @throws \yii\base\Exception
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        if($this->checkTable(self::CONTENT_CATEGORY_TABLE))
        {
            $this->createTable(self::CONTENT_CATEGORY_TABLE, [
                'id' => $this->primaryKey(),
                'title' => $this->json(),
                'alias' => $this->string(255)->notNull(),
                'desc' => $this->text(),
                'parent' => $this->tinyInteger(),

                'image' => $this->string(255),

                'template' => $this->integer(),

                'tags' => $this->json()->notNull(),
                'meta_title' => $this->string(100),
                'meta_desc' => $this->text(),

                'author' => $this->integer(),
                'created' => $this->integer(),

                'access' => $this->tinyInteger(),
                'lang' => $this->integer(),
                'state' => $this->tinyInteger()->defaultValue(0),

                'commenting' => $this->integer()->defaultValue(0),

                'created_at' => $this->integer()->notNull(),
                'updated_at' => $this->integer()->notNull(),
                'created_by' => $this->integer()->notNull(),
                'updated_by' => $this->integer()->notNull()
            ], $tableOptions);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::CONTENT_CATEGORY_TABLE);
    }
}
