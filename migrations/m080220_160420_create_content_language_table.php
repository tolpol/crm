<?php

/**
 * Class m080220_160420_create_content_languages_table
 */
class m080220_160420_create_content_language_table extends \common\db\Migration
{
    const CONTENT_LANG_TABLE = '{{%content_lang}}';

    /**
     * {@inheritdoc}
     * @throws \yii\base\Exception
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        if($this->checkTable(self::CONTENT_LANG_TABLE))
        {
            $this->createTable(self::CONTENT_LANG_TABLE, [
                'id' => $this->primaryKey(),
                'title' => $this->json()->notNull()
            ], $tableOptions);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::CONTENT_LANG_TABLE);
    }
}
