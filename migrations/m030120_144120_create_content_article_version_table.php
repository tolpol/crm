<?php

/**
 * Class m030120_144120_create_content_article_version_table
 */

class m030120_144120_create_content_article_version_table extends \common\db\Migration
{
    const CONTENT_ARTICLE_VERSION_TABLE = '{{%content_article_version}}';

    /**
     * {@inheritdoc}
     * @throws \yii\base\Exception
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        if($this->checkTable(self::CONTENT_ARTICLE_VERSION_TABLE))
        {
            $this->createTable(self::CONTENT_ARTICLE_VERSION_TABLE, [
                'id' => $this->primaryKey(),
                'article_id' => $this->integer(),
                'title' => $this->string(255),
                'alias' => $this->string(255),
                'text' => $this->text(),
                'categories' => $this->json(),

                'image1' => $this->string(255),
                'image2' => $this->string(255),
                'files' => $this->json(),
                'template' => $this->integer(),

                'tags' => $this->json(),
                'meta_title' => $this->string(100),
                'meta_desc' => $this->text(),

                'author' => $this->integer(),
                'published_at' => $this->integer(),

                'access' => $this->tinyInteger(),
                'lang' => $this->integer(),
                'state' => $this->tinyInteger()->defaultValue(0),

                'commenting' => $this->integer()->defaultValue(0),

                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
                'created_by' => $this->integer(),
                'updated_by' => $this->integer(),

                'edited_fields' => $this->json()
            ], $tableOptions);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::CONTENT_ARTICLE_VERSION_TABLE);
    }
}
