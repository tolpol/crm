<?php

namespace common\modules\keyContent\helpers;

use common\helpers\ArrayHelper;
use common\modules\keyRbac\helpers\Rbac;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

class ContentColumnHelper
{
    public static $attributes;
    public static $list;

    protected static function init($searchModel)
    {
        self::$list = [
            'checkboxColumn',
            'state',
            'title',
            'access',
            'author',
            'lang',
            'created_at',
            'id'
        ];

        self::$attributes = [
            'checkboxColumn' => ['class' => 'common\grid\CheckboxColumn'],
            'state' => [
                'attribute' => Yii::t('content', 'MODEL_CONTENT_STATE'),
                'value' => function($model) {
                    return $model->getState()[Yii::$app->language];
                },
            ],
            'title' => [
                'attribute' => Yii::t('content', 'MODEL_CONTENT_TITLE'),
                'format' => 'html',
                'value' => function($model) {
                    return '<a class="" href="' . Url::to(['update', 'id' => $model->id]) . '">' . $model->title . '</a>';
                },
            ],
            'access' => [
                'attribute' => Yii::t('content', 'MODEL_CONTENT_ACCESS'),
                'value' => function($model) {
                    return $model->getAccess()[Yii::$app->language];
                },
            ],
            'author' => [
                'attribute' => Yii::t('content', 'MODEL_CONTENT_AUTHOR'),
                'value' => function($model) {
                    return $model->getAuthor();
                },
            ],
            'lang' => [
                'attribute' => Yii::t('content', 'MODEL_CONTENT_LANG'),
                'value' => function($model) {
                    return $model->getLang()[Yii::$app->language];
                },
            ],
            'created_at' => [
                'attribute' => 'created_at',
                'value' => function($model) {
                    return date("d-m-Y  h:i:s", $model->created_at);
                },
            ],
            'id' => 'id'
        ];
    }

    public static function getColumns($searchModel, $list = [])
    {
        self::init($searchModel);
        if(empty($list))
        {
            $list = self::$list;
        }
        $columns = [];
        foreach($list as $name)
        {
            $columns[] =  self::$attributes[$name];
        }
        return $columns;
    }
}