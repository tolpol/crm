<?php

namespace common\modules\keyContent\helpers;

use common\helpers\ArrayHelper;
use common\modules\keyRbac\helpers\Rbac;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

class CategoryColumnHelper
{
    public static $attributes;
    public static $list;

    protected static function init($searchModel)
    {
        self::$list = [
            'checkboxColumn',
            'title',
            'alias',
            'parent'
        ];

        self::$attributes = [
            'checkboxColumn' => ['class' => 'common\grid\CheckboxColumn'],
            //'title' => 'title',

            'title' => [
                'attribute' => 'title',
                'format' => 'html',
                'value' =>   function($model) {
                    return '<a class="" href="' . Url::to(['update', 'id' => $model->id]) . '">' . $model->title[Yii::$app->language] . '</a>';
                },
            ],

            'alias' => 'alias',
            'parent' => 'parent'
        ];
    }

    public static function getColumns($searchModel, $list = [])
    {
        self::init($searchModel);
        if(empty($list))
        {
            $list = self::$list;
        }
        $columns = [];
        foreach($list as $name)
        {
            $columns[] =  self::$attributes[$name];
        }
        return $columns;
    }
}