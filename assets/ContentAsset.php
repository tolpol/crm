<?php

namespace common\modules\keyContent\assets;

class ContentAsset extends \common\assets\AssetBundle
{
    public $moduleID = 'keyContent';

    public $sourcePath = '@common/modules/keyContent/web';

    public $css = [
        'css/content.css',
    ];
    public $js = [
        'js/content.js',
    ];

    public $depends = [
        'backend\assets\CrmAsset'
    ];
}