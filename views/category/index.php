<?php

use yii\helpers\Html;
use common\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\keyStaff\models\search\StaffSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('staff', 'STAFF');
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = Html::a(Yii::t('staff', 'BUTTON_CREATE_EMPLOYEE'), ['/keyContent/category/create'], ['class' => 'btn btn-success white-btn cr_cont']);
?>

<div class="col-lg-12">
    <div class="card ">
        <?php \yii\widgets\Pjax::begin([
            'id' => 'staffGridView',
            'enablePushState' => false,
        ]);
        ?>
            <div class="header">
                <div class="search_tables">
                    <?= Html::beginForm(['/keyStaff/default/index'],'get', ['data-pjax' => 1]); ?>
                        <input name="StaffSearch[searchAll]" class="form-control search_field" placeholder="<?= \Yii::t('crm', 'SEARCH_PLACEHOLDER') ?>" type="text" value="<?= \common\helpers\FilterHelper::getSearchValue('Staff', 'Staff', 'searchAll') ?>">
                        <?= Html::submitButton('<i class="icon-magnifier"></i> '. \Yii::t('crm', 'BTN_SEARCH'), [
                            'class' => 'btn btn-outline-success'
                        ]) ?>
                        <button type="button" class="btn btn-outline-info showFilters"><?= \Yii::t('crm', 'BTN_FILTER') ?></button>
                    <?= Html::endForm();?>

                    <?= Html::a(\Yii::t('crm', 'BTN_CLEAR'), \yii\helpers\Url::to(['/keyStaff/default/index']), [
                        'class' => 'btn btn-outline-secondary',
                        'data-pjax' => 1,
                        'data-method' => 'POST',
                        'data-params' => [
                            'StaffSearch[clear]' => 1
                        ]
                    ]) ?>
                </div>
            </div>
            <div class="body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'gridPjaxID' => 'staffGridView',
                    'filterRowOptions' => [
                        'class' => (\common\helpers\FilterHelper::getFilterClass('Category', 'Content')) ? 'filters show_filters' : 'filters hide_filters'
                    ],
                    'rowOptions' => function ($model, $key, $index, $grid){
                        return [
                            'role' => 'row',
                            'class' => ($index % 2) ? 'odd' : 'even'
                        ];
                    },
                    'columns' => \common\modules\keyContent\helpers\CategoryColumnHelper::getColumns($searchModel),
                ]); ?>

            </div>
        <?php \yii\widgets\Pjax::end(); ?>

        <?php \yii\widgets\Pjax::begin(['id' => 'bulkPjax', 'enablePushState' => false]); ?>
            <?= \common\widgets\BulkFormWidget::widget(['url' => '/keyStaff/default/index']) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>

<?php \common\widgets\BulkWidget::registerJS($this, 'staffGridView'); ?>
