<?php

/* @var $this yii\web\View */
/* @var $model common\modules\keyContent\models\Article */

$this->title = Yii::t('staff','UPDATE_EMPLOYEE');
$this->params['breadcrumbs'][] = ['label' => Yii::t('staff','STAFF'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
?>

<?= $this->render('_form', compact(
    'model','categories', 'parentCategory', 'states',
    'categoryState', 'accesses', 'categoryAccess', 'langs', 'categoryLang', 'authors', 'categoryAuthor', 'commentings', 'categoryCommenting'
)) ?>