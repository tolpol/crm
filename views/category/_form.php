<?php

use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use common\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\modules\keyContent\models\Category */
/* @var $form common\widgets\ActiveForm */

use kartik\file\FileInput;

$form = ActiveForm::begin([
    'id' => 'category_form',
    'options' => [
        'style' => 'display: contents;'
    ]
]);

?>

<?php

if($model->isNewRecord){
    $this->params['buttons'] .= Html::submitButton(Yii::t('staff', 'BUTTON_SAVE'), ['class' => 'btn btn-success cr_cont', 'form' => 'category_form']);
}
else {
    $this->params['buttons'] .= Html::submitButton(Yii::t('staff', 'BUTTON_UPDATE'), ['class' => 'btn btn-success cr_cont', 'form' => 'category_form']);

}

?>

<div class="col-lg-6">
    <?= $form->field($model, 'title')->textInput() ?>
</div>
<div class="col-lg-2">
    <?= $form->field($model, 'alias')->textInput() ?>
</div>
<div class="col-lg-4"></div>

<div class="col-lg-12">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#materials" role="tab" aria-controls="home" aria-selected="true">Категория</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="messages-tab" data-toggle="tab" href="#messages" role="tab" aria-controls="messages" aria-selected="false">Публикация</a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content body" style="padding: 15px;background-color:#fff;">

        <!-- material block begin -->
        <div class="tab-pane active" id="materials" role="tabpanel" aria-labelledby="home-tab">
            <div class="d-flex">
                <div class="material-text w-75">
                    <?= $form->field($model, 'desc')->widget(CKEditor::className(), [
                        'options' => ['rows' => 6, 'cols' => 500, 'id' => 'editor2'],
                        'preset' => 'full',
                        'clientOptions' => [
                            'filebrowserBrowseUrl' => '/finder/index',
                        ]
                    ]); ?>
                </div>
                <div class="material-rside w-25 pl-3">

                    <div class="material-state">
                        <?= '<label class="control-label pt-2">Родительская</label>'; ?>
                        <?= Select2::widget([
                            'name' => 'parent',
                            'value' => $parentCategory,
                            'data' => $categories,
                            'options' => [
                                'multiple' => false,
                                'placeholder' => Yii::t('content', 'MODEL_CONTENT_TITLE'),
                                'required' => true
                            ],
                            'pluginOptions' => [
                                'width' => '100%'
                            ],
                        ]); ?>
                    </div>
                    <div class="material-state">
                        <?= '<label class="control-label pt-2">Состояние</label>'; ?>
                        <?= Select2::widget([
                            'name' => 'state',
                            'value' => $categoryState,
                            'data' => $states,
                            'options' => [
                                'multiple' => false,
                                'placeholder' => Yii::t('staff', 'MODEL_STAFF_POSITION_PLACEHOLDER'),
                                'required' => true
                            ],
                            'pluginOptions' => [
                                'width' => '100%'
                            ],
                        ]); ?>
                    </div>
                    <div class="material-state">
                        <?= '<label class="control-label pt-2">Доступ</label>'; ?>
                        <?= Select2::widget([
                            'name' => 'access',
                            'value' => $categoryAccess,
                            'data' => $accesses,
                            'options' => [
                                'multiple' => false,
                                'placeholder' => Yii::t('staff', 'MODEL_STAFF_POSITION_PLACEHOLDER'),
                                'required' => true
                            ],
                            'pluginOptions' => [
                                'width' => '100%'
                            ],
                        ]); ?>
                    </div>
                    <div class="material-lang">
                        <?= '<label class="control-label pt-2">Язык</label>'; ?>
                        <?= Select2::widget([
                            'name' => 'lang',
                            'value' => $categoryLang,
                            'data' => $langs,
                            'options' => [
                                'multiple' => false,
                                'placeholder' => 'language',//Yii::t('staff', 'MODEL_STAFF_POSITION_PLACEHOLDER'),
                                'required' => true
                            ],
                            'pluginOptions' => [
                                'width' => '100%'
                            ],
                        ]); ?>
                    </div>

                </div>
            </div>
        </div>
        <!-- material block end -->

        <!-- images block start-->
        <div class="tab-pane" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <div class="d-flex">

            </div>
        </div>
        <!-- images block end-->

        <!-- publish block start -->
        <div class="tab-pane" id="messages" role="tabpanel" aria-labelledby="messages-tab">
            <div class="row d-flex">
                <div class="publish-lside col-lg-3 pl-3">
                    <?php
                    $createdAt = '';
                    if ($model->getAttribute('created_at') !== null) {
                        $createdAt = date("d-m-Y  h:i:s", $model->getAttribute('created_at'));
                    }
                    ?>
                    <?= '<label class="control-label pt-2">Дата создания</label>'; ?>
                    <?= \kartik\datetime\DateTimePicker::widget([
                        'name' => 'created_at',
                        'type' => \kartik\datetime\DateTimePicker::TYPE_COMPONENT_APPEND,
                        'value' => $createdAt,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd-mm-yyyy hh:ii:ss'
                        ]
                    ]);
                    ?>

                    <?php
                    $updatedAt = '';
                    if ($model->getAttribute('updated_at') !== null) {
                        $updatedAt = date("d-m-Y  h:i:s", $model->getAttribute('updated_at'));
                    }
                    ?>
                    <?= '<label class="control-label pt-2">Дата изменения</label>'; ?>
                    <?= \kartik\datetime\DateTimePicker::widget([
                        'name' => 'updated_at',
                        'type' => \kartik\datetime\DateTimePicker::TYPE_COMPONENT_APPEND,
                        'value' => $updatedAt,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd-mm-yyyy hh:ii:ss'
                        ]
                    ]);
                    ?>

                    <?php
                    $publishedAt = '';
                    if ($model->getAttribute('published_at') !== null) {
                        $publishedAt = date("d-m-Y  h:i:s", $model->getAttribute('published_at'));
                    }
                    ?>
                    <?= '<label class="control-label pt-2">Дата публикации</label>'; ?>
                    <?= \kartik\datetime\DateTimePicker::widget([
                        'name' => 'published_at',
                        'type' => \kartik\datetime\DateTimePicker::TYPE_COMPONENT_APPEND,
                        'value' => $publishedAt,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd-mm-yyyy hh:ii:ss'
                        ]
                    ]);
                    ?>

                    <?php if(!$model->isNewRecord): ?>
                        <?= '<label class="control-label pt-2">Автор</label>'; ?>
                        <?= Select2::widget([
                            'name' => 'created_by',
                            'value' => $categoryAuthor,
                            'data' => $authors,
                            'options' => [
                                'multiple' => false,
                                'placeholder' => 'author',//Yii::t('staff', 'MODEL_STAFF_POSITION_PLACEHOLDER'),
                                'required' => true
                            ],
                            'pluginOptions' => [
                                'width' => '100%'
                            ],
                        ]); ?>
                        <label class="control-label pt-2">Изменено пользователем:</label>
                        <input type="text" disabled="disabled" class="form-control" value="<?= $authors[$model->getAttribute('updated_by')]; ?>" />
                    <?php endif; ?>

                </div>
                <div class="col-lg-3"></div>
                <div class="publish-rside col-lg-3 order-lg-2 pl-3">
                    <?= $form->field($model, 'meta_title')->textInput()->label('Meta-title'); ?>
                    <?= $form->field($model, 'meta_desc')->textInput()->label('Meta-description'); ?>

                    <?= '<label class="control-label pt-2">Комментирование</label>'; ?>
                    <?= Select2::widget([
                        'name' => 'commenting',
                        'value' => $categoryCommenting,
                        'data' => $commentings,
                        'options' => [
                            'multiple' => false,
                            'placeholder' => 'комментирование',//Yii::t('staff', 'MODEL_STAFF_POSITION_PLACEHOLDER'),
                            'required' => true
                        ],
                        'pluginOptions' => [
                            'width' => '100%'
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
        <!-- publish block end-->
    </div>
</div>



<?php ActiveForm::end(); ?>


