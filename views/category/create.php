<?php

/* @var $this yii\web\View */
/* @var $model common\modules\keyContent\models\Category */

$this->title = Yii::t('staff','CREATE_EMPLOYEE');
$this->params['breadcrumbs'][] = ['label' => Yii::t('staff','STAFF'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('_form', compact(
    'model','categories', 'parentCategory', 'states',
    'categoryState', 'accesses', 'categoryAccess', 'langs', 'categoryLang', 'commentings', 'categoryCommenting'
)) ?>