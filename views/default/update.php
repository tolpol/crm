<?php

/* @var $this yii\web\View */
/* @var $model common\modules\keyContent\models\Article */

$this->title = Yii::t('staff','UPDATE_EMPLOYEE');
$this->params['breadcrumbs'][] = ['label' => Yii::t('staff','STAFF'), 'url' => ['versionsList']];
$this->params['breadcrumbs'][] = $model->title;
?>

<?= $this->render('_form', compact(
    'model', 'categories', 'articleCategories', 'states', 'articleState', 'accesses', 'articleAccess',
            'langs', 'articleLang', 'authors', 'articleAuthor', 'tags', 'articleTags', 'commentings', 'articleCommenting'
)) ?>


<?php

if(!$model->isNewRecord){
    $this->params['buttons'] .= yii\helpers\Html::a('Версии', \yii\helpers\Url::to(['version/index', 'id' => $model->id]), [
        'class' => 'btn btn-secondary profile_actions',
        'title' => 'Версии',
    ]);
}

?>
