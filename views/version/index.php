<?php

use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use common\modules\keyContent\models\Version;

$dataProvider = new ActiveDataProvider([
    'query' => Version::findBySql('Select * from content_version where article_id = ' . $id . ' ORDER BY id DESC')
]);

?>

<div class="col-lg-12">
    <div class="card ">

        <div class="body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                     'id',
                    [
                        'label' => 'Дата',
                        'format' => 'html',
                        'value' => function($data){
                            return \yii\helpers\Html::a(
                                gmdate("Y-m-d  h:i:s", $data->created_at),
                                \yii\helpers\Url::to(['view', 'id' => $data->id])
                            );
                        },
                    ],
                    [
                        'label' => 'Редактированые поля',
                        'format' => 'text',
                        'value' => function($data){
                            if (!is_null($data->edited_fields)) {
                                return preg_replace('/[^ ,  a-zа-яё\d]/ui', ' ', json_encode($data->edited_fields));
                            }
                            return 'Создан новый материал';
                        }
                    ]
                ]
            ]);
            ?>
        </div>

    </div>
</div>