<?php

/* @var $this yii\web\View */
/* @var $model common\modules\keyContent\models\ContentVersions */
/* @var $form common\widgets\ActiveForm */

$form = \yii\widgets\ActiveForm::begin([
    'id' => 'version_form',
    'options' => [
        'style' => 'display: contents;'
    ]
]);

echo \yii\helpers\Html::submitButton('Восстановить', ['class' => 'btn btn-success cr_cont', 'form' => 'version_form']);

?>

<div class="col-lg-12">
    <div class="card ">
        <div class="body">
            <?= $form->field($model, 'id')->hiddenInput() ?>

            <?php foreach($model->getAttribute('categories') as $category): ?>
                <input type="hidden" name="categories[]" value="<?= $category ?>">
            <?php endforeach; ?>
            <?php foreach($model->getAttribute('tags') as $tag): ?>
                <input type="hidden" name="tags[]" value="<?= $tag ?>">
            <?php endforeach; ?>

            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <?php $attr = $model->getAttributes(); ?>

                <tbody>
                    <tr>
                        <td><?= 'id' ?></td>
                        <td><?= $attr['id'] ?></td>
                    </tr>
                    <tr>
                        <td><?= 'article id' ?></td>
                        <td><?= $attr['article_id'] ?></td>
                    </tr>
                    <tr>
                        <td><?= 'title' ?></td>
                        <td><?= $attr['title'] ?></td>
                    </tr>
                    <tr>
                        <td><?= 'alias' ?></td>
                        <td><?= $attr['alias'] ?></td>
                    </tr>
                    <tr>
                        <td><?= 'text' ?></td>
                        <td><?= $attr['text'] ?></td>
                    </tr>
                    <tr>
                        <td><?= 'categories' ?></td>
                        <td><?= implode(" , ", $categories); ?></td>
                    </tr>
                    <tr>
                        <td><?= 'image1' ?></td>
                        <td><?= $attr['image1'] ?></td>
                    </tr>
                    <tr>
                        <td><?= 'image2' ?></td>
                        <td><?= $attr['image2'] ?></td>
                    </tr>
                    <tr>
                        <td><?= 'files' ?></td>
                        <td><?= $attr['files'] ?></td>
                    </tr>
                    <tr>
                        <td><?= 'template' ?></td>
                        <td><?= $attr['template'] ?></td>
                    </tr>
                    <tr>
                        <td><?= 'tags' ?></td>
                        <td><?= implode(" , ", $tags); ?></td>
                    </tr>
                    <tr>
                        <td><?= 'meta title' ?></td>
                        <td><?= $attr['meta_title'] ?></td>
                    </tr>
                    <tr>
                        <td><?= 'meta description' ?></td>
                        <td><?= $attr['meta_desc'] ?></td>
                    </tr>
                    <tr>
                        <td><?= 'author' ?></td>
                        <td><?= $attr['author'] ?></td>
                    </tr>
                    <tr>
                        <td><?= 'published_at' ?></td>
                        <td><?= date("d-m-Y  h:i:s", $attr['published_at']) ?></td>
                    </tr>
                    <tr>
                        <td><?= 'access' ?></td>
                        <td><?= $attr['access'] ?></td>
                    </tr>
                    <tr>
                        <td><?= 'language' ?></td>
                        <td><?= $attr['lang'] ?></td>
                    </tr>
                    <tr>
                        <td><?= 'commenting' ?></td>
                        <td><?= $attr['commenting'] ?></td>
                    </tr>
                    <tr>
                        <td><?= 'created_at' ?></td>
                        <td><?= date("d-m-Y  h:i:s", $attr['created_at']) ?></td>
                    </tr>
                    <tr>
                        <td><?= 'updated_at' ?></td>
                        <td><?= date("d-m-Y  h:i:s", $attr['updated_at']) ?></td>
                    </tr>
                    <tr>
                        <td><?= 'created_by' ?></td>
                        <td><?= $attr['created_by'] ?></td>
                    </tr>
                    <tr>
                        <td><?= 'updated_by' ?></td>
                        <td><?= $attr['updated_by'] ?></td>
                    </tr>
                    <tr>
                        <td><?= 'edited_fields' ?></td>
                        <?php $edited = !empty($attr['edited_fields']) ? implode(', ', $attr['edited_fields']) : ''; ?>
                        <td><?= $edited ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php \yii\widgets\ActiveForm::end(); ?>