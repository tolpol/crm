<?php

namespace common\modules\keyContent\controllers;


use common\controllers\BaseController;
use common\helpers\ArrayHelper;
use common\modules\keyContent\models\Category;
use common\modules\keyContent\models\Article;
use common\modules\keyContent\models\Tag;
use common\modules\keyContent\models\Version;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Controller;

class VersionController extends Controller
{


    public function actionIndex($id)
    {
        return $this->render('index', [
            'id' => $id,
        ]);
    }

    public function actionView($id)
    {
        $model = Version::findOne($id);

        $versionCategoriesStr = implode(',' , $model->getAttribute('categories'));
        $categoriesObjArr = Category::find()->where("id IN (" . $versionCategoriesStr . ")")->all();
        $categories = ArrayHelper::map($categoriesObjArr, 'id', 'title.' . Yii::$app->language);

        $versionTagsStr = implode(',' , $model->getAttribute('tags'));
        $tagsObjArr = Tag::find()->where("id IN (" . $versionTagsStr . ")")->all();
        $tags = ArrayHelper::map($tagsObjArr, 'id', 'title.' . Yii::$app->language);

        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            $versionAttributes = $model->getAttributes();
            unset($versionAttributes['categories']);
            unset($versionAttributes['tags']);

            if ($versionAttributes['text'] === null) {
                $prevText = Version::findBySql('
                    SELECT `text` from content_version where id = (SELECT max(id) FROM content_version where `text` IS NOT NULL AND id < ' .
                    $model->id . ')')->one()->getAttribute('text');

                $versionAttributes['text'] = $prevText;
            }

            $content = Article::findOne($model->getAttribute('article_id'));
            $content->setAttributes($versionAttributes);

            if ($content->save(false))
                return $this->redirect(['default/update', 'id' => $model->getAttribute('article_id')]);
        }

        return $this->render('view', [
            'model' => $model,
            'categories' => $categories,
            'tags' => $tags
        ]);
    }
}
