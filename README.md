CRM keyContent yii2 module

VERSION: 0.0.2

DIRECTORY STRUCTURE
-------------------

```
assets/              contains module assets classes
controllers/         contains Web controller classes
i18n/                contains languages variables
migrations/          contains database migrations
models/              contains module-specific model classes
views/               contains view files for the Web application
web/                 contains the entry script
```