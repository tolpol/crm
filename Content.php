<?php

namespace common\modules\keyContent;

use common\components\LocalI18n;
use common\modules\keyContent\assets\ContentAsset;
use Yii;

/**
 * keyContent module definition class
 */
class Content extends \yii\base\Module
{
    public $moduleLangName;

    public $moduleClass;

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'common\modules\keyContent\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        if (!is_a(\Yii::$app, 'yii\console\Application') && Yii::$app->controller === null) {
            ContentAsset::register($this->view);
        }

        LocalI18n::getI18nTranslations('keyContent', 'content*');
        parent::init();
    }
}
