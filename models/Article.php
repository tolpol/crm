<?php

namespace common\modules\keyContent\models;

use common\helpers\FunctionsHelper;
use common\helpers\LangHelper;
use common\modules\keyRbac\helpers\Rbac;
use common\modules\keyStaff\helpers\AvatarHelper;
use common\modules\keyRbac\models\Permissions;
use common\modules\keyRbac\models\Roles;
use common\modules\keyStaff\models\Staff;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%content_article}}".
 *
 * @property int $id
 * @property string $title
 * @property string $alias
 * @property string $text
 * @property string $image1
 * @property string $image2
 * @property array $files
 * @property int $template
 * @property array $tags
 * @property string $metaTitle
 * @property string $metaDesc
 * @property int $author
 * @property int $created
 * @property int $publishedAt
 * @property int $access
 * @property int $lang
 * @property int $state
 * @property int $commenting
 */
class Article extends Articles
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%content_article}}';
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('staff', 'MODEL_STAFF_USERNAME'),
            'password_hash' => Yii::t('staff', 'MODEL_STAFF_PASS_HASH'),

            'full_name' => Yii::t('staff', 'MODEL_STAFF_NAME'),

            //'first_name' => Yii::t('staff', 'MODEL_STAFF_FIRST_NAME'),
            'last_name' => Yii::t('staff', 'MODEL_STAFF_LAST_NAME'),
            'middle_name' => Yii::t('staff', 'MODEL_STAFF_MIDDLE_NAME'),
            'birthday' => Yii::t('staff', 'MODEL_STAFF_BIRTHDAY'),
            'position' => Yii::t('staff', 'MODEL_STAFF_POSITION'),
            'department' => Yii::t('staff', 'MODEL_STAFF_DEPARTMENT'),
            'status' => Yii::t('staff', 'MODEL_STAFF_STATUS'),
            'staff_id' => Yii::t('staff', 'MODEL_STAFF_EMPLOYEE_ID'),
            'statusName' => Yii::t('staff', 'MODEL_STAFF_STATUS'),
            'country' => Yii::t('staff', 'MODEL_STAFF_COUNTRY'),
            'region' => Yii::t('staff', 'MODEL_STAFF_REGION'),
            'city' => Yii::t('staff', 'MODEL_STAFF_CITY'),
            'zip_number' => Yii::t('staff', 'MODEL_STAFF_ZIP_NUMBER'),
            'address' => Yii::t('staff', 'MODEL_STAFF_ADDRESS'),
            //'email' => Yii::t('staff', 'MODEL_STAFF_EMAIL'),
            'phone' => Yii::t('staff', 'MODEL_STAFF_PHONE'),
            'additional_contact' => Yii::t('staff', 'MODEL_STAFF_ADDITIONAL_CONTACT'),

            'avatar' => Yii::t('staff', 'MODEL_STAFF_AVATAR'),
            'remove_avatar' => Yii::t('staff', 'MODEL_STAFF_REMOVE_AVATAR'),

            'password' => Yii::t('staff', 'MODEL_STAFF_PASSWORD'),

            'restriction' => Yii::t('staff', 'MODEL_STAFF_RESTRICTION'),
            'ip_restriction' => Yii::t('staff', 'MODEL_STAFF_IP_RESTRICTION'),

            'role' => Yii::t('staff', 'MODEL_STAFF_ROLE'),
            'rights' => Yii::t('staff', 'MODEL_STAFF_RIGHTS'),

            'clients' => Yii::t('staff', 'MODEL_STAFF_CLIENTS'),
            'partner_code' => Yii::t('staff', 'MODEL_STAFF_PARTNER_CODE'),
            'desks' => Yii::t('staff', 'MODEL_STAFF_DESKS'),

            'ip' => Yii::t('staff', 'MODEL_STAFF_IP'),
            'created_at' => Yii::t('staff', 'MODEL_STAFF_LABEL_CREATED_AT'),
            'updated_at' => Yii::t('staff', 'MODEL_STAFF_LABEL_UPDATED_AT'),
            'created_by' => Yii::t('staff', 'MODEL_STAFF_LABEL_CREATED_BY'),
            'updated_by' => Yii::t('staff', 'MODEL_STAFF_LABEL_UPDATED_BY'),

//            'title' => Yii::t('content', 'MODEL_CONTENT_TITLE'),
//            'alias' => Yii::t('content', 'MODEL_CONTENT_ALIAS'),
//            'text' => Yii::t('content', 'MODEL_CONTENT_TEXT'),
//            'catid' => Yii::t('content', 'MODEL_CONTENT_CATEGORY'),
//            'image1' => Yii::t('content', 'MODEL_CONTENT_IMAGE1'),
//            'image2' => Yii::t('content', 'MODEL_CONTENT_IMAGE2'),
//            'files' => Yii::t('content', 'MODEL_CONTENT_FILES'),
//            'template' => Yii::t('content', 'MODEL_CONTENT_TEMPLATE'),
//            'tags' => Yii::t('content', 'MODEL_CONTENT_TAGS'),
//            'meta_title' => Yii::t('content', 'MODEL_CONTENT_META_TITLE'),
//            'meta_desc' => Yii::t('content', 'MODEL_CONTENT_META_DESC'),
//            'author' => Yii::t('content', 'MODEL_CONTENT_AUTHOR'),
//            'created' => Yii::t('content', 'MODEL_CONTENT_CREATED'),
//            'publish_up' => Yii::t('content', 'MODEL_CONTENT_PUBLISH_UP'),
//            'access' => Yii::t('content', 'MODEL_CONTENT_ACCESS'),
//            'lang' => Yii::t('content', 'MODEL_CONTENT_LANG'),
//            'state' => Yii::t('content', 'MODEL_CONTENT_STATE'),
//            'commenting' => Yii::t('content', 'MODEL_CONTENT_COMMENTING'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\modules\keyContent\models\query\ContentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\keyContent\models\query\ContentQuery(get_called_class());
    }

    public function getContentCategory() {
        return $this->hasMany(ArticleCategory::className(), ['article_id' => 'id']);
    }

    public function getCategories() {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])
            ->viaTable('content_article_category_map', ['article_id' => 'id']);
    }

    public function getTags() {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])
            ->viaTable('content_article_tag_map', ['article_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert){
        if (!isset($_POST['Version'])) {
            if($this->getAttribute('state') != $_POST['state']) {
                $this->setAttribute('state', $_POST['state']);
            }

            if($this->getAttribute('access') != $_POST['access']) {
                $this->setAttribute('access', $_POST['access']);
            }

            if($this->getAttribute('lang') != $_POST['lang']) {
                $this->setAttribute('lang', $_POST['lang']);
            }

            if($this->getAttribute('commenting') != $_POST['commenting']) {
                $this->setAttribute('commenting', $_POST['commenting']);
            }

            if($this->getAttribute('published_at') != strtotime($_POST['published_at'])) {
                $this->setAttribute('published_at', strtotime($_POST['published_at']));
            }

            if($this->getAttribute('created_at') != strtotime($_POST['created_at'])) {
                $this->setAttribute('created_at', strtotime($_POST['created_at']));
            }

            if (!$insert) {
                if($this->getAttribute('created_by') != $_POST['created_by']) {
                    $this->setAttribute('created_by', $_POST['created_by']);
                }
            }
        } else {
            $versionOnbj = new Version;
            $version = $versionOnbj::findOne($_POST['Version']['id']);

            $this->setAttribute('state', $version->getAttribute('state'));
            $this->setAttribute('access', $version->getAttribute('access'));
            $this->setAttribute('lang', $version->getAttribute('lang'));
            $this->setAttribute('commenting', $version->getAttribute('commenting'));
        }

        return parent::beforeSave($insert);
    }
    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            if (isset($_POST['categories'])) {
                foreach ($_POST['categories'] as $key => $val) {
                    $contentCategory = new ArticleCategory();
                    $contentCategory->setAttributes(['article_id' => $this->getAttribute('id'), 'category_id' => $val]);
                    $contentCategory->save();
                }
            }

            if (isset($_POST['tags'])) {
                foreach ($_POST['tags'] as $key => $val) {
                    $contentCategory = new ArticleTag();
                    $contentCategory->setAttributes(['article_id' => $this->getAttribute('id'), 'tag_id' => $val]);
                    $contentCategory->save();
                }
            }

            $version = new Version();

            $version->setAttribute('categories', $_POST['categories']);
            $version->setAttribute('tags', $_POST['tags']);
            $version->setAttribute('article_id', $this->getAttribute('id'));
            $version->setAttribute('text', $this->getAttribute('text'));

            $version->setAttributes($this->attributes);
            $version->save(false);
        } else {
            $categoriesIsChanged = $this->updateContentCategories($this->id);
            $tagsIsChanged = $this->updateContentTags($this->id);

            if ($tagsIsChanged || $categoriesIsChanged || !empty($changedAttributes)) {
                $this->createVersionOnUpdate($tagsIsChanged, $categoriesIsChanged, $changedAttributes);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $rules = [
            [['title', 'text', 'categories', 'author', 'tags'], 'required'],
            [['alias', 'published_at'], 'required'],
            [['meta_title', 'meta_desc'], 'string']
        ];

        return $rules;
    }

    private function updateContentCategories($id)
    {
        $isChanged = false;

        $contentCategoriesArr = Article::find()->where(['id' => $id])->one()->getCategories()->asArray()->all();
        $oldCategories = [];
        foreach ($contentCategoriesArr as $item) {
            $oldCategories[$item['id']] = (int)$item['id'];
        }

        $newCategories = array_flip($_POST['categories']);

        $itemsToDelete = array_diff_key($oldCategories, $newCategories);
        if (!empty($itemsToDelete)) {
            foreach ($itemsToDelete as $key => $val) {
                $contentCategory = ArticleCategory::findOne(['article_id' => $id, 'category_id' => $key]);
                $contentCategory->delete();
            }

            $isChanged = true;
        }

        $itemsToInsert = array_diff_key($newCategories, $oldCategories);
        if (!empty($itemsToInsert)) {
            foreach ($itemsToInsert as $key => $val) {
                $contentCategory = new ArticleCategory();
                $contentCategory->setAttributes(['article_id' => $id, 'category_id' => $key]);
                $contentCategory->save();
            }

            $isChanged = true;
        }

        return $isChanged;
    }

    private function updateContentTags($id)
    {
        $isChanged = false;

        $contentTagsArr = Article::find()->where(['id' => $id])->one()->getTags()->asArray()->all();
        $oldTags = [];
        foreach ($contentTagsArr as $item) {
            $oldTags[$item['id']] = (int)$item['id'];
        }

        $newTags = array_flip($_POST['tags']);

        $itemsToDelete = array_diff_key($oldTags, $newTags);
        if (!empty($itemsToDelete)) {
            foreach ($itemsToDelete as $key => $val) {
                $contentTag = ArticleTag::findOne(['article_id' => $id, 'tag_id' => $key]);
                $contentTag->delete();
            }

            $isChanged = true;
        }

        $itemsToInsert = array_diff_key($newTags, $oldTags);
        if (!empty($itemsToInsert)) {
            foreach ($itemsToInsert as $key => $val) {
                $contentTag = new ArticleTag();
                $contentTag->setAttributes(['article_id' => $id, 'tag_id' => $key]);
                $contentTag->save();
            }

            $isChanged = true;
        }

        return $isChanged;
    }

    private function createVersionOnUpdate($tagsIsChanged, $categoriesIsChanged, $changedAttributes)
    {
        $version = new Version();

        $version->setAttribute('categories', $_POST['categories']);
        $version->setAttribute('tags', $_POST['tags']);
        $version->setAttribute('article_id', $this->getAttribute('id'));

        $version->setAttributes($this->attributes);

        if (!array_key_exists('text', $changedAttributes)) {
            $version->setAttribute('text', null);
        } else {
            $version->setAttribute('text', $this->getAttribute('text'));
        }

        $editedFields = [];
        if (!empty($changedAttributes)) {
            $editedFields = array_keys($changedAttributes);
            unset($editedFields[array_search('updated_at', $editedFields)]);
            sort($editedFields);
        }

        if ($categoriesIsChanged) {
            array_push($editedFields, 'categories');
        }

        if ($tagsIsChanged) {
            array_push($editedFields, 'tags');
        }

        $version->setAttribute('edited_fields', $editedFields);

        $version->save(false);
    }

    public function getState()
    {
        return json_decode(State::find($this->state)->asArray()->one()['title'], true);
    }

    public function getAccess()
    {
        return json_decode(Access::find($this->state)->asArray()->one()['title'], true);
    }

    public function getLang()
    {
        return json_decode(Lang::find($this->state)->asArray()->one()['title'], true);
    }

    public function getAuthor()
    {
        return Staff::find($this->created_by)->asArray()->one()['username'];
    }
}

