<?php

namespace common\modules\keyContent\models;

use common\helpers\FunctionsHelper;
use common\helpers\LangHelper;
use common\modules\keyRbac\helpers\Rbac;
use common\modules\keyStaff\helpers\AvatarHelper;
use common\modules\keyRbac\models\Permissions;
use common\modules\keyRbac\models\Roles;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%content_version}}".
 *
 * @property int $id
 * @property string $title
 * @property string $alias
 * @property string $text
 * @property string $image1
 * @property string $image2
 * @property array $files
 * @property int $template
 * @property string $meta_title
 * @property string $meta_desc
 * @property int $author
 * @property int $created
 * @property int $publish_up
 * @property int $access
 * @property int $lang
 * @property int $state
 * @property int $commenting
 */
class Version extends Articles
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%content_version}}';
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('staff', 'MODEL_STAFF_USERNAME'),
            'password_hash' => Yii::t('staff', 'MODEL_STAFF_PASS_HASH'),

            'full_name' => Yii::t('staff', 'MODEL_STAFF_NAME'),

            //'first_name' => Yii::t('staff', 'MODEL_STAFF_FIRST_NAME'),
            'last_name' => Yii::t('staff', 'MODEL_STAFF_LAST_NAME'),
            'middle_name' => Yii::t('staff', 'MODEL_STAFF_MIDDLE_NAME'),
            'birthday' => Yii::t('staff', 'MODEL_STAFF_BIRTHDAY'),
            'position' => Yii::t('staff', 'MODEL_STAFF_POSITION'),
            'department' => Yii::t('staff', 'MODEL_STAFF_DEPARTMENT'),
            'status' => Yii::t('staff', 'MODEL_STAFF_STATUS'),
            'staff_id' => Yii::t('staff', 'MODEL_STAFF_EMPLOYEE_ID'),
            'statusName' => Yii::t('staff', 'MODEL_STAFF_STATUS'),
            'country' => Yii::t('staff', 'MODEL_STAFF_COUNTRY'),
            'region' => Yii::t('staff', 'MODEL_STAFF_REGION'),
            'city' => Yii::t('staff', 'MODEL_STAFF_CITY'),
            'zip_number' => Yii::t('staff', 'MODEL_STAFF_ZIP_NUMBER'),
            'address' => Yii::t('staff', 'MODEL_STAFF_ADDRESS'),
            //'email' => Yii::t('staff', 'MODEL_STAFF_EMAIL'),
            'phone' => Yii::t('staff', 'MODEL_STAFF_PHONE'),
            'additional_contact' => Yii::t('staff', 'MODEL_STAFF_ADDITIONAL_CONTACT'),

            'avatar' => Yii::t('staff', 'MODEL_STAFF_AVATAR'),
            'remove_avatar' => Yii::t('staff', 'MODEL_STAFF_REMOVE_AVATAR'),

            'password' => Yii::t('staff', 'MODEL_STAFF_PASSWORD'),

            'restriction' => Yii::t('staff', 'MODEL_STAFF_RESTRICTION'),
            'ip_restriction' => Yii::t('staff', 'MODEL_STAFF_IP_RESTRICTION'),

            'role' => Yii::t('staff', 'MODEL_STAFF_ROLE'),
            'rights' => Yii::t('staff', 'MODEL_STAFF_RIGHTS'),

            'clients' => Yii::t('staff', 'MODEL_STAFF_CLIENTS'),
            'partner_code' => Yii::t('staff', 'MODEL_STAFF_PARTNER_CODE'),
            'desks' => Yii::t('staff', 'MODEL_STAFF_DESKS'),

            'ip' => Yii::t('staff', 'MODEL_STAFF_IP'),
            'created_at' => Yii::t('staff', 'MODEL_STAFF_LABEL_CREATED_AT'),
            'updated_at' => Yii::t('staff', 'MODEL_STAFF_LABEL_UPDATED_AT'),
            'created_by' => Yii::t('staff', 'MODEL_STAFF_LABEL_CREATED_BY'),
            'updated_by' => Yii::t('staff', 'MODEL_STAFF_LABEL_UPDATED_BY'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\modules\keyContent\models\query\ContentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\keyContent\models\query\ContentQuery(get_called_class());
    }

    public function getContentCategory() {
        return $this->hasMany(ArticleCategory::className(), ['content_id' => 'id']);
    }

    public function getCategories() {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])
            ->viaTable('content_category', ['content_id' => 'id']);
    }

    public static function getPrevText($id)
    {
        return self::findBySql('SELECT `text` from content_version where id = (SELECT max(id) FROM content_version where id < ' . $id . ')');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $rules = [
            [['article_id', 'title', 'alias', 'categories', 'tags'], 'required'],
            [['meta_title', 'meta_desc'], 'string'],
            [['lang', 'access', 'published_at', 'state'], 'integer'],
        ];

        return $rules;
    }

}
