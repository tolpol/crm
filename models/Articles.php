<?php

namespace common\modules\keyContent\models;

use Yii;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

class Articles extends \common\models\ActiveRecord implements IdentityInterface {

    const STATE_ACTIVE = 1;

    public static function tableName()
    {
        return '{{%content_article}}';
    }

    /**
     * @inheritDoc
     */
    public static function findIdentity($id)
    {
        // TODO: Implement findIdentity() method.
    }

    /**
     * @inheritDoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // TODO: Implement findIdentityByAccessToken() method.
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        // TODO: Implement getId() method.
    }

    /**
     * @inheritDoc
     */
    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
    }

    /**
     * @inheritDoc
     */
    public function validateAuthKey($authKey)
    {
        // TODO: Implement validateAuthKey() method.
    }
}
