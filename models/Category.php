<?php

namespace common\modules\keyContent\models;

use common\helpers\FunctionsHelper;
use common\helpers\LangHelper;
use common\modules\keyRbac\helpers\Rbac;
use common\modules\keyStaff\helpers\AvatarHelper;
use common\modules\keyRbac\models\Permissions;
use common\modules\keyRbac\models\Roles;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%content_category}}".
 *
 * @property int $id
 * @property string $title
 * @property string $alias
 * @property string $text
 * @property int $parent
 * @property string $image1
 * @property string $image2
 * @property array $files
 * @property int $template
 * @property array $tags
 * @property string $meta_title
 * @property string $meta_desc
 * @property int $author
 * @property int $created
 * @property int $publish_up
 * @property int $access
 * @property int $lang
 * @property int $state
 * @property int $commenting
 */
class Category extends \common\models\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%content_category}}';
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('staff', 'MODEL_STAFF_USERNAME'),
            'password_hash' => Yii::t('staff', 'MODEL_STAFF_PASS_HASH'),

            'full_name' => Yii::t('staff', 'MODEL_STAFF_NAME'),

            //'first_name' => Yii::t('staff', 'MODEL_STAFF_FIRST_NAME'),
            'last_name' => Yii::t('staff', 'MODEL_STAFF_LAST_NAME'),
            'middle_name' => Yii::t('staff', 'MODEL_STAFF_MIDDLE_NAME'),
            'birthday' => Yii::t('staff', 'MODEL_STAFF_BIRTHDAY'),
            'position' => Yii::t('staff', 'MODEL_STAFF_POSITION'),
            'department' => Yii::t('staff', 'MODEL_STAFF_DEPARTMENT'),
            'status' => Yii::t('staff', 'MODEL_STAFF_STATUS'),
            'staff_id' => Yii::t('staff', 'MODEL_STAFF_EMPLOYEE_ID'),
            'statusName' => Yii::t('staff', 'MODEL_STAFF_STATUS'),
            'country' => Yii::t('staff', 'MODEL_STAFF_COUNTRY'),
            'region' => Yii::t('staff', 'MODEL_STAFF_REGION'),
            'city' => Yii::t('staff', 'MODEL_STAFF_CITY'),
            'zip_number' => Yii::t('staff', 'MODEL_STAFF_ZIP_NUMBER'),
            'address' => Yii::t('staff', 'MODEL_STAFF_ADDRESS'),
            //'email' => Yii::t('staff', 'MODEL_STAFF_EMAIL'),
            'phone' => Yii::t('staff', 'MODEL_STAFF_PHONE'),
            'additional_contact' => Yii::t('staff', 'MODEL_STAFF_ADDITIONAL_CONTACT'),

            'avatar' => Yii::t('staff', 'MODEL_STAFF_AVATAR'),
            'remove_avatar' => Yii::t('staff', 'MODEL_STAFF_REMOVE_AVATAR'),

            'password' => Yii::t('staff', 'MODEL_STAFF_PASSWORD'),

            'restriction' => Yii::t('staff', 'MODEL_STAFF_RESTRICTION'),
            'ip_restriction' => Yii::t('staff', 'MODEL_STAFF_IP_RESTRICTION'),

            'role' => Yii::t('staff', 'MODEL_STAFF_ROLE'),
            'rights' => Yii::t('staff', 'MODEL_STAFF_RIGHTS'),

            'clients' => Yii::t('staff', 'MODEL_STAFF_CLIENTS'),
            'partner_code' => Yii::t('staff', 'MODEL_STAFF_PARTNER_CODE'),
            'desks' => Yii::t('staff', 'MODEL_STAFF_DESKS'),

            'ip' => Yii::t('staff', 'MODEL_STAFF_IP'),
            'created_at' => Yii::t('staff', 'MODEL_STAFF_LABEL_CREATED_AT'),
            'updated_at' => Yii::t('staff', 'MODEL_STAFF_LABEL_UPDATED_AT'),
            'created_by' => Yii::t('staff', 'MODEL_STAFF_LABEL_CREATED_BY'),
            'updated_by' => Yii::t('staff', 'MODEL_STAFF_LABEL_UPDATED_BY'),

//            'title' => Yii::t('content', 'MODEL_CONTENT_TITLE'),
//            'alias' => Yii::t('content', 'MODEL_CONTENT_ALIAS'),
//            'text' => Yii::t('content', 'MODEL_CONTENT_TEXT'),
//            'catid' => Yii::t('content', 'MODEL_CONTENT_CATEGORY'),
//            'image1' => Yii::t('content', 'MODEL_CONTENT_IMAGE1'),
//            'image2' => Yii::t('content', 'MODEL_CONTENT_IMAGE2'),
//            'files' => Yii::t('content', 'MODEL_CONTENT_FILES'),
//            'template' => Yii::t('content', 'MODEL_CONTENT_TEMPLATE'),
//            'tags' => Yii::t('content', 'MODEL_CONTENT_TAGS'),
//            'meta_title' => Yii::t('content', 'MODEL_CONTENT_META_TITLE'),
//            'meta_desc' => Yii::t('content', 'MODEL_CONTENT_META_DESC'),
//            'author' => Yii::t('content', 'MODEL_CONTENT_AUTHOR'),
//            'created' => Yii::t('content', 'MODEL_CONTENT_CREATED'),
//            'publish_up' => Yii::t('content', 'MODEL_CONTENT_PUBLISH_UP'),
//            'access' => Yii::t('content', 'MODEL_CONTENT_ACCESS'),
//            'lang' => Yii::t('content', 'MODEL_CONTENT_LANG'),
//            'state' => Yii::t('content', 'MODEL_CONTENT_STATE'),
//            'commenting' => Yii::t('content', 'MODEL_CONTENT_COMMENTING'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\modules\keyContent\models\query\ContentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\keyContent\models\query\ContentQuery(get_called_class());
    }

    public static function getCategories($array = [])
    {
        return self::find()->select($array)->asArray()->all();
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert){
        $lang = Yii::$app->language;

        $this->setAttribute('title', [$lang => $this->getAttribute('title')]);
        $this->setAttribute('state', $_POST['state']);
        $this->setAttribute('lang', $_POST['lang']);
        $this->setAttribute('access', $_POST['access']);
        $this->setAttribute('parent', $_POST['parent']);
        $this->setAttribute('commenting', $_POST['commenting']);

        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $rules = [
            [['title', 'text'], 'required'],
            [['alias'], 'required'],
            [['parent'], 'required']
        ];

        return $rules;
    }

}
