<?php

namespace common\modules\keyContent\models\search;

use common\helpers\ArrayHelper;
use common\modules\keyContacts\models\Contact;
use common\modules\keyRbac\helpers\Rbac;
use common\modules\keyRbac\models\Roles;
use common\modules\keyStaff\models\Desks;
use common\modules\keyStaff\models\DesksStaff;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\keyContent\models\Article;

/**
 * StaffSearch represents the model behind the search form of `common\modules\keyStaff\models\Staff`.
 *
 * @property string $searchAll
 * @property string $blocked
 * @property string $deleted
 */
class ArticleSearch extends Article
{
    public $searchAll;
    public $blocked;
    public $deleted;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'state'], 'integer'],
            [['searchAll', 'blocked', 'deleted', 'statusName', 'desks', 'username', 'password_hash', 'email', 'first_name', 'last_name', 'middle_name', 'position', 'country', 'region', 'city', 'zip_number', 'address', 'phone', 'additional_contact', 'avatar', 'ip', 'ip_restriction', 'role', 'rights'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = Article::find()
            ->alias('c')
            ->addSelect([
                'c.*',
                'title' => '`c`.`title`'
            ]);



        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);


        $this->load($params);



        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if(!empty($this->searchAll))
        {
            $this->searchAll = mb_strtolower($this->searchAll);
            $query->andFilterWhere([
                'OR',
                [
                    //'staff_id' => $this->searchAll,
                    //'status' => $this->searchAll,
                    'restriction' => $this->searchAll,
                ],
                ['like', '(SELECT COUNT(*) FROM ' . Contact::tableName() . ' WHERE manager = s.staff_id)', $this->searchAll],
                ['like', '(SELECT LOWER(REPLACE(GROUP_CONCAT(CONCAT(REPLACE(`d`.`desk_lang`->"$.' . Yii::$app->language . '", \'"\', ""), "(", REPLACE(JSON_EXTRACT(\'["Employee","Head","Curator"]\', CONCAT("$[", `ds`.`desk_position`, "]")), \'"\', ""), ")")), ",", ",<br>")) FROM  ' . DesksStaff::tableName() . '  AS `ds` LEFT JOIN ' . Desks::tableName() . ' AS `d` ON (`d`.`desk_id` = `ds`.`desk_id`) WHERE `ds`.`staff_id` = `s`.`staff_id`)', $this->searchAll],
                ['like', '(LOWER(CONCAT(`s`.`first_name`, " ", `s`.`last_name`)))', $this->searchAll],
                ['like', 'username', $this->searchAll],
                ['like', 'email', $this->searchAll],
                ['like', 'middle_name', $this->searchAll],
                ['like', 'position', $this->searchAll],
                ['like', 'country', $this->searchAll],
                ['like', 'region', $this->searchAll],
                ['like', 'city', $this->searchAll],
                ['like', 'zip_number', $this->searchAll],
                ['like', 'address', $this->searchAll],
                ['like', 'phone', $this->searchAll],
                ['like', 'additional_contact', $this->searchAll],
                ['like', 'ip', $this->searchAll],
                ['like', 'ip_restriction', $this->searchAll],
                ['like', 'role', $this->searchAll],
                ['like', 'rights', $this->searchAll],
            ]);
        }




        // grid filtering conditions
        $query->andFilterWhere(['id' => $this->id])
            ->andFilterWhere(['like', 'state', $this->state]);

        return $dataProvider;
    }

    protected static function addSort(&$dataProvider, $name, $query)
    {
        $dataProvider->sort->attributes[$name] = [
            'asc' => [
                $query => SORT_ASC,
            ],
            'desc' => [
                $query => SORT_DESC,
            ],
        ];
    }
}
