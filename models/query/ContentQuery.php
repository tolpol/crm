<?php

namespace common\modules\keyContent\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\keyStaff\models\Employees]].
 *
 * @see \common\modules\keyContent\models\Articles
 */
class ContentQuery extends \yii\db\ActiveQuery
{
    /**
     * {@inheritdoc}
     * @return \common\modules\keyContent\models\Articles[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\modules\keyContent\models\Articles|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
