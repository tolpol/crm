<?php
return [
    'CONTENT' => 'Content',

    'MODEL_CONTENT_BTN_SAVE' => 'Save',
    'MODEL_CONTENT_BTN_CLOSE' => 'Close',

    'MODEL_CONTENT_TITLE' => 'title',
    'MODEL_CONTENT_ALIAS' => 'alias',
    'MODEL_CONTENT_TEXT' => 'text',
    'MODEL_CONTENT_CATEGORY' => 'category',
    'MODEL_CONTENT_IMAGE1' => 'image1',
    'MODEL_CONTENT_IMAGE2' => 'image2',
    'MODEL_CONTENT_FILES' => 'files',
    'MODEL_CONTENT_TEMPLATE' => 'template',
    'MODEL_CONTENT_TAGS' => 'tags',
    'MODEL_CONTENT_META_TITLE' => 'meta title',
    'MODEL_CONTENT_META_DESC' => 'meta description',
    'MODEL_CONTENT_AUTHOR' => 'author',
    'MODEL_CONTENT_CREATED' => 'created date',
    'MODEL_CONTENT_PUBLISH_UP' => 'publication date',
    'MODEL_CONTENT_ACCESS' => 'Access',
    'MODEL_CONTENT_LANG' => 'Language',
    'MODEL_CONTENT_STATE' => 'State',
    'MODEL_CONTENT_COMMENTING' => 'Commenting'
];