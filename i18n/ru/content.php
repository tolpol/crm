<?php
return [
    'CONTENT' => 'Контент',
    'MODEL_CONTENT_BTN_EDIT' => 'Редактировать',

    'MODEL_CONTENT_TITLE' => 'Заголовок',
    'MODEL_CONTENT_ALIAS' => 'Алиас',
    'MODEL_CONTENT_TEXT' => 'Текст',
    'MODEL_CONTENT_CATEGORY' => 'Категория',
    'MODEL_CONTENT_IMAGE1' => 'Изображение1',
    'MODEL_CONTENT_IMAGE2' => 'Изображение2',
    'MODEL_CONTENT_FILES' => 'Файлы',
    'MODEL_CONTENT_TEMPLATE' => 'Шаблон',
    'MODEL_CONTENT_TAGS' => 'Теги',
    'MODEL_CONTENT_META_TITLE' => 'Мета заголовки',
    'MODEL_CONTENT_META_DESC' => 'Мета описание',
    'MODEL_CONTENT_AUTHOR' => 'Автор',
    'MODEL_CONTENT_CREATED' => 'Дата создания',
    'MODEL_CONTENT_PUBLISH_UP' => 'Дата публикации',
    'MODEL_CONTENT_ACCESS' => 'Доступ',
    'MODEL_CONTENT_LANG' => 'Язык',
    'MODEL_CONTENT_STATE' => 'Состояние',
    'MODEL_CONTENT_COMMENTING' => 'Комментирование'
];